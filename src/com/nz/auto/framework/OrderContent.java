package com.nz.auto.framework;

import java.util.List;

/**
 *  OrderContent对象
 * @author NXQ
 *
 */
public class OrderContent {
	private String className;
	private  List<String> arguments;
	private List<String> funContent;
	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	/**
	 * @return the arguments
	 */
	public List<String> getArguments() {
		return arguments;
	}
	/**
	 * @param arguments the arguments to set
	 */
	public void setArguments(List<String> arguments) {
		this.arguments = arguments;
	}
	/**
	 * @return the funContent
	 */
	public List<String> getFunContent() {
		return funContent;
	}
	/**
	 * @param funContent the funContent to set
	 */
	public void setFunContent(List<String> funContent) {
		this.funContent = funContent;
	}

}
