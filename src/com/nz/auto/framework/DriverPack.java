package com.nz.auto.framework;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Driver类
 * 
 * @author NXQ
 *
 */
public class DriverPack {
	private WebDriver driver;

	public DriverPack() {
		setDriver();
	}

	/**
	 * @return the driver
	 */
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * @param driver the driver to set
	 */
	public void setDriver() {
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		this.driver = new ChromeDriver();
	}

}
