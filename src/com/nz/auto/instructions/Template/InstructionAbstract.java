package com.nz.auto.instructions.Template;

import com.nz.auto.framework.DriverPack;
import com.nz.auto.framework.OrderContent;

/**
 * 指令的模板
 * @author NXQ
 *
 */
public abstract class InstructionAbstract {

	/**
	 * 运行方法
	 * @return 
	 */
	 public int run(OrderContent order,DriverPack driverInit) {
		return 0;
	}
}
