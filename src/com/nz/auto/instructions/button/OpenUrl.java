package com.nz.auto.instructions.button;

import com.nz.auto.framework.DriverPack;
import com.nz.auto.framework.OrderContent;
import com.nz.auto.instructions.Template.InstructionAbstract;

/***
 * 访问URLַ
 * 
 * @author NXQ
 *
 */
public class OpenUrl extends InstructionAbstract {
			
	public  OpenUrl(OrderContent oContent,DriverPack driverInit) {
		String Url=oContent.getArguments().get(0);
		driverInit.getDriver().get(Url);
	}
}
