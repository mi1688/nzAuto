package com.nz.auto.instructions.window;



import java.io.IOException;

import org.openqa.selenium.NoSuchSessionException;

import com.nz.auto.framework.DriverPack;
import com.nz.auto.instructions.Template.InstructionAbstract;

public class closeBrowser extends InstructionAbstract{
	public int run(DriverPack driverInit) {
		driverInit.getDriver().quit();
		try {
			driverInit.getDriver().getTitle();
		} catch (NoSuchSessionException e) {
			try {
				Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return 1;
	}
	
}
