/**
 * 
 */
package com.nz.auto.instructions.struc;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.nz.auto.core.DialectMap;
import com.nz.auto.core.MultiTools;
import com.nz.auto.framework.DriverPack;
import com.nz.auto.framework.Log;
import com.nz.auto.framework.OrderContent;
import com.nz.auto.framework.WordStrcImp;
import com.nz.auto.framework.inter.FuncContent;
import com.nz.auto.instructions.Template.InstructionAbstract;

/**
 * 循环结构
 * @author NXQ
 *
 */
public class LoopStruc  extends InstructionAbstract {

	public  LoopStruc(OrderContent oContent,DriverPack driverInit) {
		String loopTimes=oContent.getArguments().get(0);
		List<String>   content=oContent.getFunContent();
		Map<String, FuncContent> map=convertContent(content);
		
		for(int i=0;i<Integer.parseInt(loopTimes);i++) {
			Log.info("循环第{}次",i+1);
			Iterator<Entry<String, FuncContent>> iterator=map.entrySet().iterator();
			while(iterator.hasNext()) {
				Entry<String, FuncContent> entry=iterator.next();
//				Log.debug(entry.getKey()+":"+entry.getValue());
				OrderContent  oContent1=new OrderContent();
				oContent1.setClassName(entry.getKey());
				oContent1.setArguments(entry.getValue().getArguments());
				oContent1.setFunContent(entry.getValue().getPreToCaluse());	
				runOrderInvoke(oContent1, driverInit);			
			}
		}
	}
	
	public Map<String, FuncContent> convertContent(List<String> preToContent) {
		MultiTools multiTools=new MultiTools();
		List<String> preList=preToContent;
		Map<String, FuncContent> map=new LinkedHashMap<String,FuncContent>();
		WordStrcImp wordStrcImp=new WordStrcImp();//词性分析器
		DialectMap dialectMap=new DialectMap();
		for(int i=0;i<preList.size();i++) {
			String content=preList.get(i);
			String or=wordStrcImp.getOrder(content);//方言指令
			String classNameKey=dialectMap.getClassName(or);
			Integer num=dialectMap.getParam(or);
			content=content.replaceAll("\\["+or+"\\]", "");
			List<String> contentList=Arrays.asList(content.split(",",num+1));
			List<String>  cList=new ArrayList<String>(contentList);
			FuncContent funcContent=new FuncContent();
			List<String> arguments=new ArrayList<String>();
			List<String> preToCaluse=new ArrayList<String>();
			for(int j=0;j<num;j++) {
				arguments.add(((String) cList.get(0)).replaceAll("\\(|\\)", ""));
				cList.remove(cList.get(0));//去除指令后的语句,列表化后个数多于参数个数,剩下数据列入子句
			}
			List<String> detailList=null;
			if(cList.size()>0) {
				//TODO: 判断括号成对匹配,嵌套 引入工具类MultiTools待完善
				detailList=multiTools.digMatchSubString(cList.get(0), 1);
				for(int j=0;j<detailList.size();j++) {
					detailList.set(j, detailList.get(j));
				}
			}		
			
			preToCaluse=detailList;
			funcContent.setArguments(arguments);
			funcContent.setPreToCaluse(preToCaluse);
			map.put(classNameKey, funcContent);			
		}
		return map;
	}
	
	public void runOrderInvoke(OrderContent oContent, DriverPack driverInit)  {
		String className = oContent.getClassName();
		try {
		Class<?> 	class1 = Class.forName(className);
		@SuppressWarnings("rawtypes")
		Class[] parameterTypes= {OrderContent.class, DriverPack.class};
		@SuppressWarnings("rawtypes")
		Constructor constructor=class1.getConstructor(parameterTypes);
		Object[] params= {oContent,driverInit};
		@SuppressWarnings("unused")
		Object object=constructor.newInstance(params);
		Thread.sleep(3000);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
