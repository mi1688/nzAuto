/**  
 * All rights Reserved, Designed By www.pcyo.cn
 * @Title:  wordStructure.java   
 * @Package com.nz.auto.framework   
 * @Description:    结构语句   
 * @author: AmoiBrush     
 * @date:   2018年7月10日 上午11:22:28   
 * @version V1.0 
 * @Copyright: 2018 www.pcyo.cn Inc. All rights reserved. 
 * 
 */
package com.nz.test.auto.inter;

/**
 * 结构接口
 * 
 * @author NXQ
 * 
 */
public interface wordInterface {
	
	String orderPattern="(\\[[\\u4e00-\\u9fa5]+\\])";
	String variablePattern="(\\{[\\u4e00-\\u9fa5a-zA-Z0-9_]+\\})";
	String calusePattern="(\\([\\s|\\S]+\\))";//子语句包
	String wordPattern="([\\u4e00-\\u9fa5a-zA-Z0-9_]+)";


	/***
	 * @Title: MultiStrc @Description: 多行结构 @param: @return: void @throws
	 */
	void MultiStrc();

	/**   
	 * @Title: singleStrc   
	 * @Description: 单行结构
	 * @param: @param s      
	 * @return: void      
	 * @throws   
	 */  
	void singleStrc(String s);
}
