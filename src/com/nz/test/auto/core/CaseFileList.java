package com.nz.test.auto.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/***
 * 用例文件列表类
 * 
 * @author NXQ
 *
 */
public class CaseFileList {

	private static List<String[]> caseList;


	/**
	 *  getCaseList
	 * @param 
	 * @return List<> String[case,data]
	 */
	public static List<String[]> getCaseList() {
		setCaseList();		
		return caseList;
	}

	protected static void setCaseList() {
		String[] caseDir = (new File("case")).list();
		List<String[]> list = new ArrayList<String[]>();
		for (String string : caseDir) {
			String casePath="case/"+string+"/";
			String initCase="init.case";//初始化入口用例
			String dataConf="data.conf";//初始化数据源
			String[] strings= {casePath+initCase,casePath+dataConf};
			list.add(strings);
		}
		if (list != null) {

		}
		caseList = list;
	}

	// Test方法
	public static void main(String[] args) {
		Log.debug(getCaseList());
		

	}

}
