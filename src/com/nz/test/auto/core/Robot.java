/**  
 * All rights Reserved, Designed By www.pcyo.cn
 * @Title:  Robot.java   
 * @Package com.nz.test.auto.core   
 * @Description:    TODO(用一句话描述该文件做什么)   
 * @author: AmoiBrush     
 * @date:   2018年8月2日 下午4:35:45   
 * @version V1.0 
 * @Copyright: 2018 www.pcyo.cn Inc. All rights reserved. 
 * 
 */
package com.nz.test.auto.core;

import java.util.List;

import com.nz.test.auto.RobotSplit;
import com.nz.test.auto.inter.OrderContent;

/**
 * 机器虫
 * @author NXQ
 *
 */
public class Robot {
	
	public void Start() {
		RobotSplit robotSplit=new RobotSplit();
		robotSplit.setDr(getRobotHead());
		robotSplit.setoContent(getRobotBody());
		robotSplit.chooseClazz();
	}
	
	
	private String className;
	private  List<String> arguments;
	private List<String> funContent;
	private DriverPack driverPack;
	
	
	public void setRobotHead(DriverPack driverPack) {
		this.driverPack=driverPack;
	}


	public DriverPack getRobotHead() {
		return driverPack;
	}
	
	public OrderContent getRobotBody() {
		OrderContent orderContent=new OrderContent();
		orderContent.setClassName(className);
		orderContent.setArguments(arguments);
		orderContent.setFunContent(funContent);
		return orderContent;
	}

	
	public void setRobotBody(String className,List<String> arguments,List<String> funContent) {
		this.className=className;
		this.arguments=arguments;
		this.funContent=funContent;
	}
	
}
