/**  
 * All rights Reserved, Designed By www.pcyo.cn
 * @Title:  wordStrcImp.java   
 * @Package com.nz.auto.framework   
 * @Description:   句法判断模型
 * @author: AmoiBrush     
 * @date:   2018年7月10日 上午11:28:43   
 * @version V1.0 
 * @Copyright: 2018 www.pcyo.cn Inc. All rights reserved. 
 * 
 */
package com.nz.test.auto.core;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.nz.auto.core.DialectMap;
import com.nz.auto.framework.inter.wordInterface;


/***
 * 句法结构体
 * 
 * @author NXQ
 *
 */
public class WordStrcImp implements wordInterface {

	/**
	 * <p>
	 * Title: singleStrc
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @see com.nz.auto.framework.inter.wordInterface#singleStrc()
	 */
	@Override
	public void singleStrc(String s) {
		s = " [打开]{abc} 	\n";
		String string = s.replaceAll(" |	|\r|\n", "");
		String pattern = "^" + orderPattern + "(" + variablePattern + "|" + wordPattern + ")+" + "$";
		// 创建pattern对象
		Pattern re = Pattern.compile(pattern);
		// 创建matcher对象
		Matcher matcher = re.matcher(string);
		Log.debug(matcher.groupCount());

		if (matcher.find()) {
			for (int i = 0; i <= matcher.groupCount(); i++) {

				Log.debug("分组数{} 分组{} {}", matcher.groupCount(), i, matcher.group(i));
			}
		}

	}

	/**
	 * 获取指令 @Title: getOrder @Description: clause @param: @return @return:
	 * String @throws
	 */
	public String getOrder(String clause) {
		if (clause == "") {
			clause = "[循环]3,([打开]www.baidu.com)";
		}
		String string = clause.replaceAll(" |	|\r|\n", "");
		String pattern = orderPattern;
		String rtnOrder = "";
		Matcher matcher = Pattern.compile(pattern).matcher(string);
//		Log.debug(matcher.groupCount());
		if (matcher.find()) {
			for (int i = 0; i <= matcher.groupCount(); i++) {
				rtnOrder = matcher.group(i).replaceAll("\\[|\\]", "");
//			Log.debug("捕捉的指令:"+rtnOrder);
			}
		} else {
//			rtnOrder=null;
			Log.debug("捕捉的指令为空:");
		}
//		Log.debug("语句:{} ,指令:{}",clause,rtnOrder);
		return rtnOrder;
	}

	/**
	 * 返回子句 @Title: getOrder @Description: clause @param: @return @return:
	 * String @throws
	 */
	public String RemoveOrder(String clause, String order) {
		clause = clause.replaceAll("[" + order + "]", "");
		return clause;
	}

	/**
	 * 获取变量 @Title: getVariable @Description: clause @param: @return @return:
	 * String @throws
	 */
	public List<String> getVariable(String clause) {
		if (clause == "") {
			clause = " [打开]{ABC}333{KK}";
		}
		String string = clause.replaceAll(" |	|\r|\n", "");
		String pattern = variablePattern;
		List<String> rtnOrder = new ArrayList<>();
		Matcher matcher = Pattern.compile(pattern).matcher(string);
//		int count=0;
		while(matcher.find()) {
//			count+=1;
			rtnOrder.add(matcher.group().replaceAll("\\{|\\}", ""));
//			Log.debug(matcher.groupCount()+" "+rtnOrder);
		}
//		Log.debug("语句:{} ,匹配到{},个,变量:{}", clause, count,rtnOrder);
		return rtnOrder;
	}

	/**
	 * 获取子语句包 @Title: getClause @Description: clause @param: @return @return:
	 * String @throws
	 */
	public String getClause(String clause) {
		clause = " [点击]([CSS]\"a[title=1]\")".replaceAll(" |	|\r|\n", "");
		System.out.println(clause);
		String string = clause.replace("\\", "");
		Log.debug("替换..." + string);
		String pattern = "^[\\s|\\S]*" + calusePattern + "[\\s|\\S]*$";
		String rtnOrder = "";
		Matcher matcher = Pattern.compile(pattern).matcher(string);
		Log.debug(matcher.groupCount());
		if (matcher.find()) {
			for (int i = 0; i <= matcher.groupCount(); i++) {
				rtnOrder = matcher.group(i).replaceAll("\\(|\\)", "");
				Log.debug(rtnOrder);
			}
		}
		return rtnOrder;
	}

	/**
	 * <p>
	 * Title: MultiStrc
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @see com.nz.auto.framework.inter.wordInterface#MultiStrc()
	 */
	@Override
	public void MultiStrc() {
		// TODO 多行结构

	}

	/**
	 * 判断case传入行
	 */
	public int judgeStructure(String singleLineText) {
		DialectMap dialectMap = new DialectMap();
		String ck = getOrder(singleLineText);
		int complexity = dialectMap.getInstructionComplexity(ck);
		return complexity;
	}

	/**
	 * 判断case传入行-测试方法
	 */
	public int judgeStructure() {
		String singleLineText = "[打开]{testUrl}";
		DialectMap dialectMap = new DialectMap();
		String ck = getOrder(singleLineText);
		int complexity = dialectMap.getInstructionComplexity(ck);
		if (complexity == 1) {
			String caluse = RemoveOrder(singleLineText, ck);

		}
		return complexity;
	}

	// 测试方法
	public static void main(String[] args) {
		WordStrcImp wordStrcImp = new WordStrcImp();
//		wordStrcImp.singleStrc("");
//		wordStrcImp.judgeStructure();
		wordStrcImp.getOrder("");
		wordStrcImp.getVariable("");
//		wordStrcImp.getClause("");
//		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
//		Gson gson=new Gson();
//		Log.debug("at: "+stackTrace[1].getClassName()+"."+stackTrace[1].getMethodName()+"("+stackTrace[1].getFileName()+":"+stackTrace[1].getLineNumber()+")");
	}

}
