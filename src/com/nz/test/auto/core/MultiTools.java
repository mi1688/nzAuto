/**  
 * All rights Reserved, Designed By www.pcyo.cn
 * @Title:  multiTools.java   
 * @Package com.nz.auto.core   
 * @Description:    TODO(用一句话描述该文件做什么)   
 * @author: AmoiBrush     
 * @date:   2018年7月31日 下午6:20:33   
 * @version V1.0 
 * @Copyright: 2018 www.pcyo.cn Inc. All rights reserved. 
 * 
 */
package com.nz.test.auto.core;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * @author NXQ
 *
 */
public class MultiTools {

	
	/**
	 *  圆括号成对匹配,返回所有匹配组,以层级建立
	 * @return 
	 * @return 
	 */
	public  LinkedHashMap<String, List<Point>> ParenthesesMatch(String input) {
		if(input=="") {
			input="hello (java(abc1)),((abc2),(abc3),(abc4))";
		}
		Integer len=input.length();
		LinkedHashMap<String, List<Point>> levelMap=new LinkedHashMap<>();
		List<String> position=new ArrayList<String>();
		int level=1;//层级
		for(int i=0;i<len;i++) {
			char c=input.charAt(i);
			if(c=='(') {
				position.add(level+"L"+i);
				level+=1;
			}else if(c==')') {
				level-=1;
				position.add(level+"R"+i);				
			}
		}
		for(int i=0;i<position.size();i++) {
			String string=position.get(i);//层级位置
			if(string.indexOf("L")!=-1) {
				String[] subString=string.split("L");
				String yString="";
				for(int j=i;j<position.size();j++) {
					if(position.get(j).indexOf(subString[0]+"R")!=-1) {
						yString=position.get(j);
						break;
					}
				}
				Point point=new Point();
				point.setPoint(string.split("L")[1], yString.split("R")[1]);
				List<Point> listPoint=levelMap.get(subString[0]);
				if(listPoint ==null) {
					listPoint=new ArrayList<Point>();
				}
				listPoint.add(point);
				levelMap.put(subString[0], listPoint);
			}
		}
		
//		Log.info(position);
//		Log.info(levelMap);
		return levelMap;
		
	}
	
	/**
	 *  获取指定字符串的几级嵌套子字符串组
	 * @return 
	 * @return 
	 */
	public  List<String> digMatchSubString(String input,int level) {
		if(input=="") {
			input="hello (java(abc1)),((abc2),(abc3),(abc4))";
		}
		List<Point> positionList=ParenthesesMatch(input).get(String.valueOf(level));//获取对应层级的匹配位置
		List<String> sliceString=new ArrayList<>();
		for(Point position:positionList) {
			sliceString.add(input.substring(position.getX(), position.getY()+1));
		}
		
//		Log.info(sliceString);
		return  sliceString;
		
	}
	
	
	/**   
	 * @Title: main   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param args      
	 * @return: void      
	 * @throws  
	 */
	public static void main(String[] args) {
		MultiTools multiTools=new MultiTools();
//		multiTools.ParenthesesMatch("");
		multiTools.digMatchSubString("", 1);
		multiTools.digMatchSubString("", 2);
//		String input="hello (java(abc1)),((abc2),(abc3),(abc4))";
//		Log.info("{}{}",input.length(),input.subSequence(6,17));
	}
	
	
	 class Point{
		private  Integer x;
		private  Integer y;
		/**  
		 * @Title:  getX <BR>  
		 * @Description: please write your description <BR>  
		 * @Field Integer <BR>  
		 * @return the x
		 */
		public  Integer getX() {
			return x;
		}
		/**  
		 * @Title:  getY <BR>  
		 * @Description: please write your description <BR>  
		 * @Field Integer <BR>  
		 * @return the y
		 */
		public  Integer getY() {
			return y;
		}
		
		public  void setPoint(Integer startIndex,Integer EndIndex) {
			this.x=startIndex;
			this.y=EndIndex;
		}

		public void setPoint(String startIndex,String EndIndex) {
			this.x=Integer.parseInt(startIndex);
			this.y=Integer.parseInt(EndIndex);
		}
	}


}
