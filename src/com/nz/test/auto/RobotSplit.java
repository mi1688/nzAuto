package com.nz.test.auto;


import java.util.ArrayList;
import java.util.List;



import com.nz.auto.framework.Log;
import com.nz.test.auto.core.DriverPack;
import com.nz.test.auto.instruction.ClickButton;
import com.nz.test.auto.instruction.OpenUrl;
import com.nz.test.auto.instruction.struc.LoopStruc;
import com.nz.test.auto.inter.OrderContent;
import com.nz.test.auto.inter.Runner;
/***
 * 分拣中心
 * @author NXQ
 *
 */
public class RobotSplit {


	private OrderContent oContent;
	private DriverPack dr;
	/**  
	 * @Title:  getoContent <BR>  
	 * @Description: please write your description <BR>  
	 * @Field OrderContent <BR>  
	 * @return the oContent
	 */
	public OrderContent getoContent() {
		return oContent;
	}



	/**  
	 * @Title:  setoContent <BR>  
	 * @Description: please write your description <BR>  
	 * @return: OrderContent <BR>  
	 * @param oContent the oContent to set
	 */
	public void setoContent(OrderContent oContent) {
		this.oContent = oContent;
	}



	/**  
	 * @Title:  getDr <BR>  
	 * @Description: please write your description <BR>  
	 * @Field DriverPack <BR>  
	 * @return the dr
	 */
	public DriverPack getDr() {
		return dr;
	}



	/**  
	 * @Title:  setDr <BR>  
	 * @Description: please write your description <BR>  
	 * @return: DriverPack <BR>  
	 * @param driverInit the dr to set
	 */
	public void setDr(DriverPack driverInit) {
		this.dr = driverInit;
	}



	public void chooseClazz() {
		String className=oContent.getClassName();
		Runner runner = null;
		if(className==null || className=="") {
			className="com.nz.test.auto.ClickButton";
		}
		switch (className) {
		case "com.nz.test.auto.instruction.struc.LoopStruc":
			runner=new LoopStruc();
			break;
		case "com.nz.test.auto.instruction.ClickButton":
			runner=new ClickButton();
			break;
		case "com.nz.test.auto.instruction.OpenUrl":
			runner=new OpenUrl();
			break;
		default:
			break;
		}
		try {
			Control control=new Control(runner,getoContent(),getDr());
			control.dealCase();
		}catch (NullPointerException e) {
			Log.error("找不到对应的执行指令实现类{}", className);
			throw e;
		} 
		
	}
	
	

	
	public static void main(String[] args) {
		RobotSplit testRun=new RobotSplit();
		testRun.chooseClazz();
	}





}
