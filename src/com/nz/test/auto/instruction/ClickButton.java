package com.nz.test.auto.instruction;


import com.nz.test.auto.core.InstructionStruc;
import com.nz.test.auto.inter.CallBack;
import com.nz.test.auto.inter.Runner;

public class ClickButton extends InstructionStruc implements Runner {


	@Override
	public void processDetail(CallBack callBack) {
		try {
			this.setContent(callBack.getContent());
			this.setDriverPack(callBack.getDriver());
			this.getContent();
			this.getDriverPack();
			Thread.sleep(1000);
		} catch (Exception e) {
		}
		callBack.returnResult("点击按钮");
	}


}
